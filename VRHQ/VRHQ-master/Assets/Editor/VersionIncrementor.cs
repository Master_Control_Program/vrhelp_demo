﻿
// Inspired by http://forum.unity3d.com/threads/automatic-version-increment-script.144917/

using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using UnityEditor.Build;

[InitializeOnLoad]
public class VersionIncrementor : ResourceSingleton<VersionIncrementor>, IPreprocessBuild
{
    public int MajorVersion;
    public int MinorVersion = 1;
    public int BuildVersion;
    public string CurrentVersion;

    public int callbackOrder { get { return 0; } }

    [PostProcessBuild(1)]
    public static void OnPreprocessBuild2(BuildTarget target, string pathToBuiltProject)
    {
        //Debug.Log("Build v" + Instance.CurrentVersion);
        // IncreaseBuild();
    }

    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        Debug.Log("Build v" + Instance.CurrentVersion);
        IncreaseBuild();
    }

    void IncrementVersion(int majorIncr, int minorIncr, int buildIncr)
    {
        MajorVersion += majorIncr;
        MinorVersion += minorIncr;
        BuildVersion += buildIncr;

        UpdateVersionNumber();
       
    }


    [MenuItem("Build/Create Version File")]
    private static void Create()
    {
        Instance.Make();
    }

    [MenuItem("Build/Increase Major Version")]
    private static void IncreaseMajor()
    {
        Instance.MajorVersion++;
        Instance.MinorVersion = 0;
        Instance.BuildVersion = 0;
        Instance.UpdateVersionNumber();
    }
    [MenuItem("Build/Increase Minor Version")]
    private static void IncreaseMinor()
    {
        Instance.MinorVersion++;
        Instance.BuildVersion = 0;
        Instance.UpdateVersionNumber();
    }

    private static void IncreaseBuild()
    {
        Instance.BuildVersion++;
        Instance.UpdateVersionNumber();
    }

    void UpdateVersionNumber()
    {
        //Make your custom version layout here.
        CurrentVersion = MajorVersion.ToString("0") + "." + MinorVersion.ToString("00") + "." + BuildVersion.ToString("000");

        PlayerSettings.Android.bundleVersionCode = MajorVersion * 10000 + MinorVersion * 1000 + BuildVersion;
        PlayerSettings.bundleVersion = CurrentVersion;
        EditorUtility.SetDirty(Instance);
    }
}

public abstract class ResourceSingleton<T> : ScriptableObject
       where T : ScriptableObject
{
    private static T m_Instance;
    const string AssetPath = "Assets/Resources";

    public static T Instance
    {
        get
        {
            if (ReferenceEquals(m_Instance, null))
            {
                m_Instance = Resources.Load<T>("VersionIncrementor");
                //m_Instance = Resources.Load<ScriptableObject>(AssetPath + "VersionIncrementor");

#if UNITY_EDITOR
                if (m_Instance == null)
                {
                    //Debug.LogError("ResourceSingleton Error: Fail load at " + "Singletons/" + typeof(T).Name);
                    CreateAsset();
                }
                else
                {
                    //Debug.Log("ResourceSingleton Loaded: " + typeof (T).Name);
                }
#endif
                var inst = m_Instance as ResourceSingleton<T>;
                if (inst != null)
                {
                    inst.OnInstanceLoaded();
                }
            }
            return m_Instance;
        }
    }

    public void Make() { }
    static void CreateAsset()
    {
        m_Instance = ScriptableObject.CreateInstance<T>();
        string path = Path.Combine(AssetPath, typeof(T).ToString() + ".asset");
        path = AssetDatabase.GenerateUniqueAssetPath(path);
        AssetDatabase.CreateAsset(m_Instance, path);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = m_Instance;
    }

    protected virtual void OnInstanceLoaded()
    {
    }
}
