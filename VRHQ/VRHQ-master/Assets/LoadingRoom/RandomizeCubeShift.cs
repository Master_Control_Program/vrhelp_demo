﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RandomizeCubeShift : MonoBehaviour {

    public float min = -1.0f;
    public float max = 1.0f;
    public float timeToTravel = 1.0f;
    public bool xWall = false;

    private List<Transform> _transforms = new List<Transform>();

    private void Awake()
    {
        DOTween.SetTweensCapacity(1500, 5);

        for (int i = 0; i != transform.childCount; i++)
        {
            float rand = Random.Range(min, max);

            //print(rand);

            if (xWall)
            {
                transform.GetChild(i).DOLocalMoveX(rand, timeToTravel).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            } else
            {
                transform.GetChild(i).DOLocalMoveZ(rand, timeToTravel).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            }
            
            _transforms.Add(transform.GetChild(i));
        }
    }
}