﻿/* 
 * Headless Builder
 * (c) Salty Devs, 2018
 * 
 * Please do not publish or pirate this code.
 * We worked really hard to make it.
 * 
 */

using System;
using System.Reflection;
using System.Collections;
using System.Linq;
using UnityEngine;

// This class finds all callbacks and allows other scripts to call them
public class HeadlessCallbacks : Attribute {

	private static IEnumerable callbackRegistry = null;

	public static void FindCallbacks() {
		if (callbackRegistry != null) {
			return;
		}

		callbackRegistry =
			from a in AppDomain.CurrentDomain.GetAssemblies ()
		 	from t in a.GetTypes ()
		 	let attributes = t.GetCustomAttributes (typeof(HeadlessCallbacks), true)
		 	where attributes != null && attributes.Length > 0
		 	select t;
	}

	public static void InvokeCallbacks(string callbackName) {
		FindCallbacks ();

		foreach (Type type in callbackRegistry) {
			MethodInfo callbackMethod = type.GetMethod (callbackName);
			if (callbackMethod != null) {
				try {
					callbackMethod.Invoke (type, null);
				} catch (Exception e) {
					Debug.LogError (e);
				}
			}
		}
	}

}