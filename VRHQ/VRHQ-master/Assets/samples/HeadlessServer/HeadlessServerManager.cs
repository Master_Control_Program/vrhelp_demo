﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;
using Bolt;
using Bolt.Samples.Photon.Simple;
using Bolt.Photon;
using UdpKit;

public class HeadlessServerManager : Bolt.GlobalEventListener
{
    public string Map = "";
    public string GameType = "";
    public string RoomID = "";

    void Awake()
    {
        BoltLauncher.SetUdpPlatform(new PhotonPlatform());
    }

    public override void BoltStartBegin()
    {
        if (IsHeadlessMode() == true)
        {
            BoltNetwork.RegisterTokenClass<RoomProtocolToken>();
            BoltNetwork.RegisterTokenClass<ServerAcceptToken>();
            BoltNetwork.RegisterTokenClass<ServerConnectToken>();

            // Custom properties Token
            BoltNetwork.RegisterTokenClass<PhotonRoomProperties>();
        
            XRSettings.enabled = false;
            Application.targetFrameRate = 60;
            QualitySettings.vSyncCount = 0;
            ConsoleWriter.Open();
        }
    }

    public override void BoltStartDone()
    {
        if (IsHeadlessMode() == true)
            if (BoltNetwork.IsServer)
            {
                // Start the Server
                RoomProtocolToken token = new RoomProtocolToken()
                {
                    ArbitraryData = Map
                };

                string matchName = "MyPhotonGame #" + UnityEngine.Random.Range(1, 100);

                BoltNetwork.SetServerInfo(matchName, token);
                BoltNetwork.LoadScene(Map);
            }
    }

    public override void Connected(BoltConnection connection)
    {
        BoltLog.Warn("Connected");

        ServerAcceptToken acceptToken = connection.AcceptToken as ServerAcceptToken;

        if (acceptToken != null)
        {
            BoltLog.Info("AcceptToken: " + acceptToken.GetType());
            BoltLog.Info("AcceptToken: " + acceptToken.data);
        }
        else
        {
            BoltLog.Warn("AcceptToken is NULL");
        }

        ServerConnectToken connectToken = connection.ConnectToken as ServerConnectToken;

        if (connectToken != null)
        {
            BoltLog.Info("ConnectToken: " + connectToken.GetType());
            BoltLog.Info("ConnectToken: " + connectToken.data);
        }
        else
        {
            BoltLog.Warn("ConnectToken is NULL");
        }
    }

    public override void ConnectRequest(UdpEndPoint endpoint, IProtocolToken token)
    {
        BoltConsole.Write("SOMEONE IS TRYING TO CONNECT");

        base.ConnectRequest(endpoint, token);
        //token should be ServerConnectToken

        if (token != null)
        {
            BoltConsole.Write("GOT A TOKEN " + token.GetType().ToString());
            BoltLog.Warn(token.GetType().ToString());

            ServerConnectToken t = token as ServerConnectToken;
            BoltLog.Warn("Server Token: null? " + (t == null));
            BoltLog.Warn("Data: " + t.data);
            BoltConsole.Write("Client Version: " + t.data);
            BoltConsole.Write("Server Version: " + Application.version);

            if (t.data != Application.version)
            {
                BoltNetwork.Refuse(endpoint);
                BoltConsole.Write("VERSION IS INCORRECT");
            }
            else
            {
                ServerAcceptToken acceptToken = new ServerAcceptToken
                {
                    data = "Accepted"
                };

                BoltConsole.Write("VERSION IS CORRECT");
                BoltNetwork.Accept(endpoint, acceptToken);
            }
        }
        else
        {
            BoltLog.Warn("Received token is null");
        }
    }

    public override void ConnectRefused(UdpEndPoint endpoint, IProtocolToken token)
    {
        BoltLog.Warn("Connect Refused");
        base.ConnectRefused(endpoint, token);
    }

    // Use this for initialization
    void Start()
    {
        if (IsHeadlessMode() == true)
        {

            // Get custom arguments from command line
            Map = GetArg("-m", "-map") ?? Map;
            GameType = GetArg("-t", "-gameType") ?? GameType; // ex: get game type from command line
            RoomID = GetArg("-r", "-room") ?? RoomID;

            // Validate the requested Level
            var validMap = false;

            foreach (string value in BoltScenes.AllScenes)
            {
                if (SceneManager.GetActiveScene().name != value)
                {
                    if (Map == value)
                    {
                        validMap = true;
                        break;
                    }
                }
            }

            if (!validMap)
            {
                BoltLog.Error("Invalid configuration: please verify level name");
                Application.Quit();
            }

            BoltLauncher.StartServer();
            DontDestroyOnLoad(this);
        }
    }
    /// <summary>
    /// Utility function to detect if the game instance was started in headless mode.
    /// </summary>
    /// <returns><c>true</c>, if headless mode was ised, <c>false</c> otherwise.</returns>
    public static bool IsHeadlessMode()
    {
        return Environment.CommandLine.Contains("-batchmode") && Environment.CommandLine.Contains("-nographics");
    }

    static string GetArg(params string[] names)
    {
        var args = Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            foreach (var name in names)
            {
                if (args[i] == name && args.Length > i + 1)
                {
                    return args[i + 1];
                }
            }
        }

        return null;
    }
}
