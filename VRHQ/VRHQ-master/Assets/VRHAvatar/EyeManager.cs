﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EyeManager : MonoBehaviour {

    [SerializeField] private GameObject[] eyeObjects;
    [SerializeField] private float targetEyeSize = 0.001f;
    [SerializeField] private float originalEyeSize = 0.03f;
    [SerializeField] private float delayTime = 3.5f;
    [SerializeField] private float timeToRun = 0.15f;

    private Sequence _mySequence = null;

    private void Start()
    {
        foreach (GameObject eye in eyeObjects)
        {
            _mySequence = DOTween.Sequence();

            _mySequence.Append(eye.transform.DOScaleY(targetEyeSize, timeToRun)
            .SetDelay(delayTime)
            .SetEase(Ease.Linear));

            _mySequence.Append(eye.transform.DOScaleY(originalEyeSize, timeToRun)
            .SetEase(Ease.Linear))
            .SetDelay(delayTime);

            _mySequence.SetLoops(-1, LoopType.Restart);
        }
    }
}