﻿Shader "Custom/Portal" {
	Properties{
	  _MainTex("Texture", 2D) = "white" {}
	  _Cube("Cubemap", CUBE) = "" {}
	  _Scale("Cubemap Depth Scale", float) = 1.0
	}
		SubShader{
		  Tags { "RenderType" = "Opaque" }
		  CGPROGRAM
		  #pragma surface surf Lambert
		  struct Input {
			  float2 uv_MainTex;
			  float3 viewDir;
			  float3 worldNormal;
		  };
		  sampler2D _MainTex;
		  samplerCUBE _Cube;
		  float _Scale;
		  void surf(Input IN, inout SurfaceOutput o) {
			  o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * 0.5;
			  float3 cubeDirection = lerp(IN.worldNormal, IN.viewDir, _Scale);
			  o.Emission = texCUBE(_Cube, cubeDirection).rgb;
		  }
		  ENDCG
	  }
		  Fallback "Diffuse"
}