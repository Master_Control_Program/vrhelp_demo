﻿using UnityEngine;

public static class StaticData
{
    public static Color PlayerColor = Color.black;
    public static bool Restarting = false;
    public static string CurrentScene = string.Empty;
    public static string LoginStatus = string.Empty;
}
