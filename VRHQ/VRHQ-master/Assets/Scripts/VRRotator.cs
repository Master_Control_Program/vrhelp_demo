﻿using UnityEngine;

public class VRRotator : MonoBehaviour
{
    public GameObject rig;
    public GameObject centerEye;
    public bool enableRotation = true;
    public float rotationSpeed = 1.0f;

    void Update()
    {
        if (enableRotation)
        {
            if (OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x >= 0.01f)
            {
                rig.transform.RotateAround(centerEye.transform.position, Vector3.up, rotationSpeed);
            } else if (OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x <= -0.01f)
            {
                rig.transform.RotateAround(centerEye.transform.position, Vector3.down, rotationSpeed);
            }
        }
    }
}