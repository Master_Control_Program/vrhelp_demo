﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRHCallbacks : Bolt.GlobalEventListener
{
    public override void SceneLoadLocalDone(string scene)
    {
        StaticData.CurrentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        if (BoltNetwork.IsClient)
        {
            BoltNetwork.Instantiate(BoltPrefabs.PlayerEntity);
        }
    }
}
