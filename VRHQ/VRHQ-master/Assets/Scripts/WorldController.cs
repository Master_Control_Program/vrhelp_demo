﻿using Bolt;
using Bolt.Photon;
using Bolt.Samples.Photon.Simple;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldController : Bolt.GlobalEventListener
{
    public override void BoltStartBegin()
    {
        BoltNetwork.RegisterTokenClass<RoomProtocolToken>();
        BoltNetwork.RegisterTokenClass<ServerAcceptToken>();
        BoltNetwork.RegisterTokenClass<ServerConnectToken>();

        // Custom properties Token
        BoltNetwork.RegisterTokenClass<PhotonRoomProperties>();
    }

    public string submittedWorldName = "meetingroom";

    public override void BoltShutdownBegin(AddCallback registerDoneCallback)
    {
        if (BoltNetwork.IsClient)
        {
            registerDoneCallback(GoToLoadingRoom);
        }
    }

    void GoToLoadingRoom()
    {
        SceneManager.LoadScene("LoadingRoom");
    }

    private void OnTriggerEnter(Collider other)
    {
        print("Shutting Down for Swap");
        PlayerPrefs.SetString("LoadingWorldName", submittedWorldName);
        if (BoltNetwork.IsRunning)
        {
            BoltNetwork.ShutdownImmediate();
        } else
        {
            SceneManager.LoadScene("LoadingRoom");
        }
    }
}
