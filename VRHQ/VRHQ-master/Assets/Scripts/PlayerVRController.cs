﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerVRController : Bolt.EntityEventListener<IPlayerVRState>
{
    public Transform MyHead;
    public Transform MyBody;
    public Transform MyLeftHand;
    public Transform MyRightHand;
    public Transform MyName;
    public GameObject MyShirt;
    public Text NameplateText;
    public Vector3 nameplatePosOffset;
    public Vector3 nameplateRotOffset;
    RigFinder MyRigFinder;

    public override void Attached()
    {
        state.AddCallback("shirtColor", ColorChanged);
        state.AddCallback("playerInt", NameChanged);

        if (entity.IsOwner)
        {
            MyRigFinder = GameObject.Find("RigFinder").GetComponent<RigFinder>();

            state.SetTransforms(state.head, MyRigFinder.head.transform);
            state.SetTransforms(state.body, MyRigFinder.body.transform);
            state.SetTransforms(state.leftHand, MyRigFinder.leftHand.transform);
            state.SetTransforms(state.rightHand, MyRigFinder.rightHand.transform);

            if (StaticData.PlayerColor == Color.black)
            {
                StaticData.PlayerColor = new Color(Random.value, Random.value, Random.value);
            }

            state.shirtColor = StaticData.PlayerColor;

            if (BoltNetwork.IsClient)
            {
                state.playerInt = (int)BoltNetwork.Server.ConnectionId;
            }
        }
        else
        {
            state.SetTransforms(state.head, MyHead);
            state.SetTransforms(state.body, MyBody);
            state.SetTransforms(state.leftHand, MyLeftHand);
            state.SetTransforms(state.rightHand, MyRightHand);
        }
    }

    private void LateUpdate()
    {
        MyName.position = (MyBody.position + nameplatePosOffset);
        MyName.eulerAngles = (MyBody.eulerAngles + nameplateRotOffset);
    }

    void ColorChanged()
    {
        MyShirt.GetComponent<Renderer>().material.color = state.shirtColor;
    }

    void NameChanged()
    {
        NameplateText.text = "Player " + state.playerInt.ToString();
    }
}
