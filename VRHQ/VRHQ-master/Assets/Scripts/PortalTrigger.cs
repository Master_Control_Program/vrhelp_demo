﻿using System;
using UdpKit;
using UnityEngine;

namespace Bolt.Samples.GettingStarted
{
    public class PortalTrigger : Bolt.GlobalEventListener
    {
        public string sceneName = "meetingroom";

        void OnTriggerEnter (Collider other)
        {
            BoltLauncher.StartClient();
        }

        public override void SessionListUpdated(Map<Guid, UdpSession> sessionList)
        {
            Debug.LogFormat("Session list updated: {0} total sessions", sessionList.Count);

            foreach (var session in sessionList)
            {
                if (session.Value.HostName == sceneName)
                {
                    UdpSession photonSession = session.Value as UdpSession;

                    if (photonSession.Source == UdpSessionSource.Photon)
                    {
                        BoltNetwork.Connect(photonSession);
                    }
                }
            }
        }
    }
}
