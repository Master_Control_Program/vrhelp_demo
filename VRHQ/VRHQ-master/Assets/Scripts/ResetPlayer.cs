﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlayer : MonoBehaviour
{
    public float yBreakPoint = -5.0f;
    public Transform spawnPoint;

    private void Update()
    {
        if (transform.position.y > yBreakPoint)
        {
            transform.position = spawnPoint.position;
        }
    }
}
