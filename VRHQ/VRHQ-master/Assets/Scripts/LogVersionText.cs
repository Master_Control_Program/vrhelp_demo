﻿using UnityEngine;
using UnityEngine.UI;

public class LogVersionText : MonoBehaviour
{
    void Start()
    {
        GetComponent<Text>().text = "Version " + Application.version;
    }
}