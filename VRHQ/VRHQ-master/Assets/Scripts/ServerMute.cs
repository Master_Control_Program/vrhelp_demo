﻿using Dissonance;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerMute : Bolt.GlobalEventListener
{
    DissonanceComms DC;

    private void Start()
    {
        if (BoltNetwork.IsServer)
        {
            DC = GetComponent<DissonanceComms>();
            DC.IsMuted = true;
        }
    }
}
