﻿using Bolt.Photon;
using Bolt.Samples.Photon.Simple;
using System;
using udpkit.platform.photon;
using UdpKit;
using UnityEngine;
using UnityEngine.XR;

namespace Bolt.Samples.GettingStarted
{
    public class AdminControls : Bolt.GlobalEventListener
    {
        public string sceneName = "meetingroom";

        public void StartServer(string name)
        {
            sceneName = name;
            BoltLauncher.StartServer();
            DontDestroyOnLoad(this);
        }

        public void StartClient(string name)
        {
            sceneName = name;
            BoltLauncher.StartClient();
        }

        public override void BoltStartBegin()
        {
            BoltNetwork.RegisterTokenClass<RoomProtocolToken>();
            BoltNetwork.RegisterTokenClass<ServerAcceptToken>();
            BoltNetwork.RegisterTokenClass<ServerConnectToken>();

            // Custom properties Token
            BoltNetwork.RegisterTokenClass<PhotonRoomProperties>();

            XRSettings.enabled = false;
            Application.targetFrameRate = 60;
            QualitySettings.vSyncCount = 0;
        }

        void Awake()
        {
            BoltLauncher.SetUdpPlatform(new PhotonPlatform());
        }

        public override void BoltStartDone()
        {
            if (BoltNetwork.IsServer)
            {
                // Start the Server
                RoomProtocolToken token = new RoomProtocolToken()
                {
                    ArbitraryData = sceneName,
                    password = "123",
                };

                string matchName = "MyPhotonGame #" + UnityEngine.Random.Range(1, 100);
                BoltConsole.Write(Application.version + " THIS IS MY VERSION");
                BoltNetwork.SetServerInfo(matchName, token);
                BoltNetwork.LoadScene(sceneName);
            }
        }

        public override void Connected(BoltConnection connection)
        {
            BoltLog.Warn("Connected");

            ServerAcceptToken acceptToken = connection.AcceptToken as ServerAcceptToken;

            if (acceptToken != null)
            {
                BoltLog.Info("AcceptToken: " + acceptToken.GetType());
                BoltLog.Info("AcceptToken: " + acceptToken.data);
            }
            else
            {
                BoltLog.Warn("AcceptToken is NULL");
            }

            ServerConnectToken connectToken = connection.ConnectToken as ServerConnectToken;

            if (connectToken != null)
            {
                BoltLog.Info("ConnectToken: " + connectToken.GetType());
                BoltLog.Info("ConnectToken: " + connectToken.data);
            }
            else
            {
                BoltLog.Warn("ConnectToken is NULL");
            }
        }

        public override void ConnectRequest(UdpEndPoint endpoint, IProtocolToken token)
        {
            BoltConsole.Write("SOMEONE IS TRYING TO CONNECT");

            base.ConnectRequest(endpoint, token);
            //token should be ServerConnectToken

            if (token != null)
            {
                BoltConsole.Write("GOT A TOKEN " + token.GetType().ToString());
                BoltLog.Warn(token.GetType().ToString());

                ServerConnectToken t = token as ServerConnectToken;
                BoltLog.Warn("Server Token: null? " + (t == null));
                BoltLog.Warn("Data: " + t.data);
                BoltConsole.Write("Client Version: " + t.data);
                BoltConsole.Write("Server Version: " + Application.version);

                if (t.data != Application.version)
                {
                    BoltNetwork.Refuse(endpoint);
                    BoltConsole.Write("VERSION IS INCORRECT");
                } else
                {
                    ServerAcceptToken acceptToken = new ServerAcceptToken
                    {
                        data = "Accepted"
                    };
                    
                    BoltConsole.Write("VERSION IS CORRECT");
                    BoltNetwork.Accept(endpoint, acceptToken);
                }
            }
            else
            {
                BoltLog.Warn("Received token is null");
            }
        }

        public override void ConnectRefused(UdpEndPoint endpoint, IProtocolToken token)
        {
            BoltLog.Warn("Connect Refused");
            base.ConnectRefused(endpoint, token);
        }

        public override void SessionListUpdated(Map<Guid, UdpSession> sessionList)
        {
            Debug.LogFormat("Session list updated: {0} total sessions", sessionList.Count);

            foreach (var session in sessionList)
            {
                if (session.Value.HostName == sceneName)
                {
                    UdpSession photonSession = session.Value as UdpSession;

                    if (photonSession.Source == UdpSessionSource.Photon)
                    {
                        BoltNetwork.Connect(photonSession);
                    }
                }
            }
        }
    }
}