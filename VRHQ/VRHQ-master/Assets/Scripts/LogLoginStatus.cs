﻿using UnityEngine;
using UnityEngine.UI;

public class LogLoginStatus : MonoBehaviour
{
    private void Start()
    {
        if (StaticData.LoginStatus == string.Empty)
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                StaticData.LoginStatus = "Offline";
            }
            else if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
            {
                StaticData.LoginStatus = "Online via Data";
            } else if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
            {
                StaticData.LoginStatus = "Online via Wifi";
            }
        }

        GetComponent<Text>().text = StaticData.LoginStatus;
    }
}
