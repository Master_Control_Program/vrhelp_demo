﻿using UnityEngine;

public class SlideEventListener : Bolt.GlobalEventListener
{
    public GameObject[] slides = null;

    public bool next, prev = false;

    private int _slideIndex = 0;

    private void Update()
    {
        if (next)
        {
            next = false;
            NextSlide();
        }
        if (prev)
        {
            prev = false;
            PrevSlide();
        }
    }

    public void NextSlide()
    {
        _slideIndex++;
        if (_slideIndex > 23)
        {
            _slideIndex = 0;
        }
        var evnt = SlideEvent.Create(Bolt.GlobalTargets.Everyone);
        evnt.Index = _slideIndex;
        evnt.Send();
    }

    public void PrevSlide()
    {
        _slideIndex--;
        if (_slideIndex < 0)
        {
            _slideIndex = 23;
        }
        var evnt = SlideEvent.Create(Bolt.GlobalTargets.Everyone);
        evnt.Index = _slideIndex;
        evnt.Send();
    }

    public override void OnEvent(SlideEvent evnt)
    {
        for (int i = 0; i < slides.Length; i++)
        {
            slides[i].SetActive(false);
        }

        slides[evnt.Index].SetActive(true);
    }
}
