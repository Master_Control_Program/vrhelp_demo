﻿using UdpKit;

namespace Bolt.Samples.Photon.Simple
{
    public class ServerReconnect : Bolt.GlobalEventListener
    {
        public int counter;

        public override void BoltShutdownBegin(AddCallback registerDoneCallback)
        {
            StaticData.Restarting = true;
        }

        public override void ConnectRequest(UdpEndPoint endpoint, IProtocolToken token)
        {
            BoltNetwork.Accept(endpoint);
        }
        
        void Update()
        {
            if (StaticData.Restarting == true)
            {
                counter++;

                if (counter > 1000)
                {
                    BoltLauncher.StartServer();
                }
            }

        }

        public override void BoltStartDone()
        {
            StaticData.Restarting = false;

            if (BoltNetwork.IsServer)
            {
                BoltNetwork.SetServerInfo(StaticData.CurrentScene, null);
                BoltNetwork.LoadScene(StaticData.CurrentScene);
            }
        }
    }
}