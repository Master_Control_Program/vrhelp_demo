﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Dissonance.Demo
{
    public class NameplateSpeakerIndicator : MonoBehaviour
    {
        public Image _indicator;

        private float _intensity;

        private IDissonancePlayer _player;
        private VoicePlayerState _state;

        private bool IsSpeaking
        {
            get { return _player.Type == NetworkPlayerType.Remote && _state != null && _state.IsSpeaking; }
        }

        private void Start()
        {
            //Find the component attached to this game object which marks it as a Dissonance player representation
            _player = GetComponent<IDissonancePlayer>();

            StartCoroutine(FindPlayerState());
        }

        private IEnumerator FindPlayerState()
        {
            //Wait until player tracking has initialized
            while (!_player.IsTracking)
                yield return null;

            //Now ask Dissonance for the object which represents the state of this player
            //The loop is necessary in case Dissonance is still initializing this player into the network session
            while (_state == null)
            {
                _state = FindObjectOfType<DissonanceComms>().FindPlayer(_player.PlayerId);
                yield return null;
            }
        }

        private void Update()
        {
            if (IsSpeaking)
            {
                //Calculate intensity of speech - do the pow to visually boost the scale at lower intensities
                _intensity = Mathf.Max(Mathf.Clamp(Mathf.Pow(_state.Amplitude, 0.175f), 0.25f, 1), _intensity - Time.deltaTime);
                print("Speaking");
                _indicator.color = new Color(158, 210, 255,_intensity);
            }
            else
            {
                //Fade out intensity when player is not talking
                _intensity -= Time.deltaTime * 2;
            }
        }
    }
}
