﻿using UnityEngine;
using System;
using UdpKit;
using Bolt.Utils;
using udpkit.platform.photon;
using UnityEngine.SceneManagement;
using System.Collections;
using Bolt.Photon;

namespace Bolt.Samples.Photon.Simple
{
    public class InitAuto : Bolt.GlobalEventListener
    {
        private bool _weHaveStartedToConnect = false;
        private bool connected;

        private void Log(string msg)
        {
            Debug.Log("InitAuto.cs MESSAGE --> " + msg);
        }

        public override void SessionListUpdated(Map<Guid, UdpSession> sessionList)
        {
            if (_weHaveStartedToConnect == false)
            {
                print("Starting Timeout");

                foreach (var session in sessionList)
                {
                    print(session.Value.HostName);

                    // Simple session
                    UdpSession udpSession = session.Value as UdpSession;

                    // Skip if is not a Photon session
                    if (udpSession.Source != UdpSessionSource.Photon)
                        continue;

                    // Photon Session
                    PhotonSession photonSession = udpSession as PhotonSession;

                    string sessionDescription = String.Format("{0} / {1} ({2})",
                                photonSession.Source, photonSession.HostName, photonSession.Id);

                    print(sessionDescription);

                    RoomProtocolToken token = photonSession.GetProtocolToken() as RoomProtocolToken;

                    if (token != null)
                    {
                        sessionDescription += String.Format(" :: {0}", token.ArbitraryData);
                    }
                    else
                    {
                        object value_t = -1;
                        object value_m = -1;

                        if (photonSession.Properties.ContainsKey("t"))
                        {
                            value_t = photonSession.Properties["t"];
                        }

                        if (photonSession.Properties.ContainsKey("m"))
                        {
                            value_t = photonSession.Properties["m"];
                        }

                        sessionDescription += String.Format(" :: {0}/{1}", value_t, value_m);
                    }

                    print(Application.version);
                    print(PlayerPrefs.GetString("LoadingWorldName"));
                    print(photonSession.HostName);

                    if (token.ArbitraryData == PlayerPrefs.GetString("LoadingWorldName"))
                    {
                        print(PlayerPrefs.GetString("LoadingWorldName"));

                        ServerConnectToken connectToken = new ServerConnectToken
                        {
                            data = Application.version
                        };

                        BoltNetwork.Connect(photonSession, connectToken);
                        Invoke("DoAutoShutdown", 8f);
                        _weHaveStartedToConnect = true;

                        break;
                    }
                }
            }
        }

        public override void Connected(BoltConnection connection)
        {
            connected = true;
        }

        public override void ConnectRefused(UdpEndPoint endpoint, IProtocolToken token)
        {
            base.ConnectRefused(endpoint, token);
            SceneManager.LoadScene(0);
            StaticData.LoginStatus = "Incorrect Version. Please download new Version of VRH";
        }

        public void DoAutoShutdown()
        {
            if (connected == false)
                BoltNetwork.Shutdown();
        }

        public override void BoltShutdownBegin(AddCallback registerDoneCallback)
        {
            if (connected == false)
            {
                registerDoneCallback(Test0);
            }
        }

        void Test0()
        {
            BoltLauncher.StartClient();
        }

        private void Start()
        {
            Log("Auto Starting Client");
            if (!BoltNetwork.IsRunning)
            {
                BoltLauncher.StartClient();
            } else
            {
                BoltNetwork.Shutdown();
            }
        }

        public override void BoltStartBegin()
        {
            BoltNetwork.RegisterTokenClass<RoomProtocolToken>();
            BoltNetwork.RegisterTokenClass<ServerAcceptToken>();
            BoltNetwork.RegisterTokenClass<ServerConnectToken>();

            // Custom properties Token
            BoltNetwork.RegisterTokenClass<PhotonRoomProperties>();
        }
    }
}