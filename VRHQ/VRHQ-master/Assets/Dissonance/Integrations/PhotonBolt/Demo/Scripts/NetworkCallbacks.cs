﻿using UnityEngine;

namespace Dissonance.Integrations.PhotonBolt.Demo
{
    //[BoltGlobalBehaviour]
    public class NetworkCallbacks : Bolt.GlobalEventListener
    {
        public override void SceneLoadLocalDone(string map)
        {
            // randomize a position
            var pos = new Vector3(Random.Range(-4, 4), 0, Random.Range(-4, 4));

            // instantiate cube
            BoltNetwork.Instantiate(BoltPrefabs.Bolt_Player_Prefab, pos, Quaternion.identity);
        }
    }
}
