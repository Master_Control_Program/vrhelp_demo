﻿namespace Dissonance.Integrations.PhotonBolt.Demo
{
    public class DissonancePlayerTracking : BoltPlayer<IPlayerVRState>
    {
        public DissonancePlayerTracking()
            : base("DissonancePlayerId", state => state.DissonancePlayerId, (state, id) => state.DissonancePlayerId = id)
        {
        }
    }
}
