﻿using System.Collections;
using UnityEngine;


public class Draw : MonoBehaviour
{
    public bool Thick;

    public float lineDist = 0.01f;

    Vector3 oldPosition;

    public bool drawing;

    [SerializeField] Transform targetFollow;

    [SerializeField] private GameObject paintObject;
    private GameObject _go;
    private LineRenderer _line;
    private ApplyColor _ac;
    private int num = 1;
    private Coroutine co;

    public void StartLine()
    {
        if (drawing == false)
        {
            MakeLine();
            co = StartCoroutine(DrawLine());

            drawing = true;
        }
    }

    public void EndLine()
    {
        if (drawing == true)
        {
            _line.GetComponent<PaintEntityController>().donezo = true;
            _line.GetComponent<PaintEntityController>().StartDestroyTimer(1f);

            StopCoroutine(co);
            _go = null;
            _line = null;
            num = 1;

            drawing = false;
        }
    }

    private IEnumerator DrawLine()
    {
        while (true)
        {
            if (Vector3.Distance(transform.position, oldPosition) > lineDist)
            {
                if (num > 995)
                {
                    EndLine();
                }
                else
                {
                    oldPosition = transform.position;
                    _line.positionCount = num;
                    _line.SetPosition(num - 1, transform.position);
                    _line.GetComponent<PaintEntityController>().state.positions[num - 1] = transform.position;
                    num++;
                }


            }
            yield return null;
        }
    }

    void MakeLine()
    {
        _go = BoltNetwork.Instantiate(BoltPrefabs.Paint, new Vector3(0, 0, 0), Quaternion.identity);

        _line = _go.GetComponent<LineRenderer>();

        _go.GetComponent<PaintEntityController>().state.color = transform.gameObject.GetComponent<Renderer>().material.color;

        if (Thick == true)
        {
            _go.GetComponent<PaintEntityController>().state.width = 3;
        }
    }
}
