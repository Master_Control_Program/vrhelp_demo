﻿using UnityEngine;


[RequireComponent(typeof(BoxCollider))]
public class ColorColliderData : MonoBehaviour
{
    public Color myColor;
}
