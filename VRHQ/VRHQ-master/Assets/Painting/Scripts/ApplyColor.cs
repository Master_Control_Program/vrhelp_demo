﻿using UnityEngine;

public class ApplyColor : MonoBehaviour
{
    public Renderer[] renderersToChange;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("ColorBlock"))
        {
            foreach(Renderer rend in renderersToChange)
            {
                rend.material.color = collider.GetComponent<Renderer>().material.color;
            }
        }
    }
}