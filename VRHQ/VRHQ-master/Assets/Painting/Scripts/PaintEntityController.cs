﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintEntityController : Bolt.EntityEventListener<IPaintState>
{
    public PaintEntityController newPEC;


    public int createdBy;


    public bool serverEntityMode;

    BoltEntity ServerEntity;

    //color ?
    public bool donezo;

    LineRenderer LR;
    int confirmedIndex = 0;

    public override void Attached()
    {
        GetComponent<LineRenderer>().numCornerVertices = 5;

        if (BoltNetwork.IsServer)
        {
            if (entity.isOwner == false)
            {
                if (serverEntityMode == false)
                {

                    ServerEntity = BoltNetwork.Instantiate(BoltPrefabs.PaintServer);

                    if (entity.source != null)
                        ServerEntity.GetComponent<PaintEntityController>().state.source = (int)entity.source.ConnectionId;

                    ServerEntity.GetComponent<PaintEntityController>().state.color = state.color;
                    ServerEntity.GetComponent<PaintEntityController>().state.width = state.width;
                    ServerEntity.GetComponent<PaintEntityController>().createdBy = (int)entity.source.ConnectionId;
                    ServerEntity.GetComponent<PaintEntityController>().state.sourceEntity = entity;

                }
            }
        }

        if (BoltNetwork.IsClient == true && serverEntityMode == true)
        {
            if (entity.isOwner == false)
            {
                if (state.source == (int)BoltNetwork.Server.ConnectionId)
                {
                    entity.GetComponent<LineRenderer>().enabled = false;

                    //ServerEntity.GetComponent<LineRenderer>().enabled = true;

                }


            }
        }



        LR = GetComponent<LineRenderer>();

        if (serverEntityMode == true)
            state.AddCallback("positions[]", OnStatsChanged);
        else
            state.AddCallback("positions[]", OnStatsChanged);

        state.AddCallback("color", Test0);

        state.AddCallback("width", Test1);

        Test0();

    }

    public void StartDestroyTimer(float time)
    {

        if (serverEntityMode == false && entity.isOwner)
        {

            //Invoke("DestroyMe", time);

        }

    }

    void DestroyMe()
    {
        if (BoltNetwork.IsRunning)
        {
            if (entity.isOwner)
            {
                ServerEntity.GetComponent<LineRenderer>().enabled = true;
                BoltNetwork.Destroy(entity);
            }
        }
    }

    public void Test0()
    {
        LR.material.color = state.color;
    }


    public void Test1()
    {
        LR.widthMultiplier = state.width;
    }

    public void OnStatsChanged(Bolt.IState state, string path, Bolt.ArrayIndices indices)
    {
        IPaintState actorState = (IPaintState)state;
        int index = indices[0];

        if (serverEntityMode == false)
        {
            if (BoltNetwork.IsServer && entity.isOwner == false)
            {
                if (ServerEntity == null)
                    Debug.Log("AAAA");

                else ServerEntity.GetComponent<PaintEntityController>().state.positions[index] = actorState.positions[index];
            }
            return;

        }
        else
        {
            Debug.Log("qweqw");

        }

        //we get new number here
        //first we want to check if this is adjacent to the highest confirmed index
        //otherwise we ignore for now

        //if adjacent we setposition for this number and all sequential numbers that have arrived until we get to a blank 
        //we update the confirmed number







        if (index < 1000)
        {
            if (confirmedIndex == index)
            {
                LR.positionCount = index + 1;
                LR.SetPosition(index, actorState.positions[index]);
                index++;



                while (actorState.positions[index].x != 0)
                {
                    LR.positionCount = index + 1;
                    LR.SetPosition(index, actorState.positions[index]);
                    index++;
                }

                confirmedIndex = index;
            }
        }



        // The changed property:
        // actorState.stats[index]
        //actorState.positions[index].

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
