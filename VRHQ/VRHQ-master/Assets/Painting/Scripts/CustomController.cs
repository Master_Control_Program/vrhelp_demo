﻿using System.Collections;
using UnityEngine;
public class CustomController : MonoBehaviour
{
    [Header("Painting References")]
    public GameObject marker;
    public GameObject palette;
    public PaintController PC;
    ColorColliderData CCD;

    //public Animator handAnimator;
    
    GameObject _grabbableObj, _grabbedObj = null;

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            Debug.Log("DoTriggerPressed");
            if (marker.activeSelf == true && _grabbedObj != null)
            {
                if (PC.MyDraw.drawing == false)
                    PC.MyDraw.StartLine();
            }
        }
        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger))
        {
            Debug.Log("DoTriggerReleased");
            if (marker.activeSelf == true)
            {
                if (PC.MyDraw.drawing == true)
                    PC.MyDraw.EndLine();
            }
        }
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
        {
            Debug.Log("DoGripPressed");
            //handAnimator.SetBool("MakeFist", true);
            CheckForGrab();
        }
        if (OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger))
        {
            Debug.Log("DoGripReleased");
            //handAnimator.SetBool("MakeFist", false);
            if (_grabbedObj != null)
            {
                ReleaseGrab();
            }
        }
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            Debug.Log("DoButtonOnePressed");
            marker.SetActive(!marker.activeSelf);
            palette.SetActive(!palette.activeSelf);
        }
        if (OVRInput.GetDown(OVRInput.Button.Four))
        {
            var evnt = DestroyAllLines.Create(Bolt.GlobalTargets.OnlyServer);
            evnt.Send();
            DestroyAllMyLines();
            Debug.Log("DoButtonTwoPressed");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Grabbable"))
        {
            _grabbableObj = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_grabbableObj != null)
        {
            _grabbableObj = null;
        }
    }

    private void DestroyAllMyLines()
    {
        if (marker.activeSelf == true)
        {
            foreach (BoltEntity entity in BoltNetwork.Entities)
            {
                if (entity.IsOwner)
                {
                    PaintEntityController PC;
                    PC = entity.GetComponent<PaintEntityController>();
                    if (PC != null)
                    {
                        if (PC.donezo == true)
                        {
                            BoltNetwork.Destroy(entity);
                        }
                    }
                }

            }
        }
    }
    
    private void CheckForGrab()
    {
        if (_grabbableObj != null)
        {
            _grabbedObj = _grabbableObj;
            _grabbedObj.transform.parent = transform;
        }
    }

    private void ReleaseGrab()
    {
        if (_grabbedObj != null)
        {
            _grabbedObj.transform.parent = null;
            _grabbedObj = null;
        }
    }
}
