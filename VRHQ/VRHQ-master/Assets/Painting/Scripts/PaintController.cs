﻿using UnityEngine;

public class PaintController : Bolt.GlobalEventListener
{
    public Draw MyDraw;
    public ApplyColor AC;
    
    public override void OnEvent(DestroyAllLines evnt)
    {
        if (BoltNetwork.IsServer)
        {
            int fromID;

            if (evnt.FromSelf == true)
                fromID = 0;
            else fromID = (int)evnt.RaisedBy.ConnectionId;

            foreach (BoltEntity entity in BoltNetwork.Entities)
            {
                if (entity.IsOwner)
                {
                    PaintEntityController PC;
                    PC = entity.GetComponent<PaintEntityController>();
                    if (PC != null)
                    {
                        if (PC.createdBy == fromID)
                        {
                            BoltNetwork.Destroy(entity);
                        }
                    }
                }

            }


        }
    }

    // Update is called once per frame
    void Update()
    {

        //draw line


        if (Input.GetKeyDown(KeyCode.Q))
        {
            MyDraw.EndLine();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            MyDraw.StartLine();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            //  MyDraw.transform.Translate(Vector3.for)
        }
    }
}
